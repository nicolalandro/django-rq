from redis import Redis
from rq import Queue

q = Queue('worker', connection=Redis())

from rq_task import count_words_at_string

job = q.enqueue(count_words_at_string, 'http://nvie.com', at_front=True,)

print(job.result)

import time
time.sleep(2)
print(job.result)
