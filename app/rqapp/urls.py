from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create', views.CreateAPI.as_view(), name='create'),
    path('questions', views.QuestionsAPI.as_view(), name='create'),
]
