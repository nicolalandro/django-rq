from django.apps import AppConfig


class RqappConfig(AppConfig):
    name = 'rqapp'
