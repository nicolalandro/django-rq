import django_rq
from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Question
from .rq_task import create_question


class IndexView(TemplateView):
    template_name = 'index.html'


class CreateAPI(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        text = request.data['text']

        queue = django_rq.get_queue('default')
        queue.enqueue(create_question, text=text, at_front=True)

        return Response(status=status.HTTP_200_OK)


class QuestionsAPI(APIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        questions = Question.objects.all()
        response = {'data': [q.question_text for q in questions]}
        return Response(response)
