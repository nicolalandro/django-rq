from django.contrib import admin

from .models import Question, Choice


# class QuestionAdmin(admin.ModelAdmin):
#     list_display = ['question_text']
#     search_fields = ['question_text']
#
#
# admin.site.register(QuestionAdmin, Question)

admin.site.register(Question)
admin.site.register(Choice)

