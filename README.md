# Use Redis Queue with Django
This is a sample project that use rq with django models.

## Requirements
* install python 3.7
* install redis
* (optionally install postgres)

## Create venv
```
mkdir django-rq
cd django-rg
python3.7 -m venv venv
```

repete this into every terminal
```
cd django-rg
source venv/bin/activate
```

## Create django app
```
pip install django
django-admin startproject app
pip freeze > requirements.txt
```

## Create app
```
cd app
python manage.py startapp rqapp
```

## Generate migration and apply
```
cd app
python manage.py makemigrations rqapp
python manage.py migrate
```

## Migrate & Run server
```
cd app
python manage.py migrate
python manage.py runserver
firefox http://localhost:8000/rqapp/
```

## Show urls
```
python manage.py show_urls
```

## Create superuser
```
cd app
python manage.py createsuperuser
```

## Simple Redis & rq

* run redis
```
redis-server
```
* run worker
```
cd rq_simple
rq worker worker
```
* run command
```
cd rq_simple
python rq_task_runner.py
```

## Rq django
* [ref](https://github.com/rq/django-rq)
* pip install django-rq
* pip freeze > requirements.txt

### Run

* run redis
```
redis-server
```
* vrun worker
```
python manage.py rqworker default
```
* run django
```
# run django
firefox http://localhost:8000/django-rq/
```

# Demo
* run server and rq-djano
* firefox http://localhost:8000/rqapp/
* wirte Text
* press button Enqueue (add a question by rq)
* press button Update (show question)
